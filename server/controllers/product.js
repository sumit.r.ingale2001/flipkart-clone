import Product from '../models/productSchema.js'
import User from '../models/userSchema.js'

export const addProducts = async (req, res) => {
    try {
        const data = req.body;
        const products = new Product(data);
        await products.save();
        res.status(200).json(products);
    } catch (error) {
        res.status(500).json(error, 'error while adding products');
    }
}

export const getProducts = async (req, res) => {
    try {
        const products = await Product.find();
        res.status(200).json(products);
    } catch (error) {
        res.status(500).json(error, 'error while getting the products');
    }
}

// function to get single product 
export const getProduct = async (req, res) => {
    try {
        const { id } = req.params;
        const product = await Product.findOne({ "id": id });
        res.status(200).json(product);
    } catch (error) {
        res.status(500).json(error, "error while getting product")
    }
}

export const addToCart = async (req, res) => {
    try {
        const { userId, id } = req.body;
        const user = await User.findById(userId);
        const item = await Product.findById(id)
        if (!user) {
            return res.json({ info: "Login to add products" })
        } else {
            user.cart.push(item);
            await user.save()
            return res.json({ success: "Product added", cart: user.cart })
        }
    } catch (error) {
        res.json(error, { error: "Something went wrong try again later" })
    }
}