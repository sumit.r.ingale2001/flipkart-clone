import User from '../models/userSchema.js'
import bcrypt from 'bcryptjs';
import dotenv from 'dotenv'
import jwt from 'jsonwebtoken'

dotenv.config();
const SECRET_KEY = process.env.SECRET_KEY;


const hashedPassword = async (pass) => {
    const salt = await bcrypt.genSalt(10);
    const hashPass = await bcrypt.hash(pass, salt);
    return hashPass;
}

const comparePassword = async (userPass, databasePass) => {
    const comparedPassword = await bcrypt.compare(userPass, databasePass);
    return comparedPassword;
}

export const signUp = async (req, res) => {
    try {

        const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
        const exist = await User.findOne({ username: req.body.username });
        if (exist) {
            return res.status(401).json({ message: "User already exist" });
        }
        else {
            const { firstName, lastName, username, email, password, phone } = req.body;
            if (!emailRegex.test(email)) {
                return res.json({ error: "Invalid email format" })
            }
            if (password.length < 6) {
                return res.json({ warning: "Password must be greater than 5 character" })
            }
            const hashedPass = await hashedPassword(password);
            const user = new User({
                password: hashedPass,
                firstName,
                lastName,
                username,
                email,
                phone
            })
            await user.save()
            return res.status(200).json({ message: "User registered successfully", username: user.username })
        }

    } catch (error) {
        res.status(500).json(error, "error while sining up the user ")
    }
}

export const loginUser = async (req, res) => {
    try {
        const { email, password } = req.body;
        const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;

        const user = await User.findOne({ email });
        if (!user) {
            return res.json({ error: "User doesnt exist please sign in" });
        }
        else {
            if (!password || password.length < 6) {
                return res.json({ warning: "Password must be greater than 5 character" })
            } else if (!emailRegex.test(email)) {
                return res.json({ email: "Invalid email format" })
            } else {
                const isValid = await comparePassword(password, user.password);
                if (!isValid) {
                    return res.json({ error: "Incorrect password" })
                } else {
                    const token = jwt.sign({ email: user.email, userID: user._id }, SECRET_KEY, { expiresIn: "1d" })
                    return res.json({ message: "Logged in successfully", token: token, userID: user._id, username: user.username, email: user.email })
                }
            }
        }
    } catch (error) {
        res.status(500).json(error, 'error while logging in the user');
    }
}

export const verfiyUser = async (req, res) => {
    try {
        res.json({ message: "Verified user" })
    } catch (error) {
        res.json(error, "error while verifying the user");
    }
}

export const verify = async (req, res, next) => {
    try {
        const token = req.cookies.access_token;
        if (!token) {
            return res.json({ error: "Token missing" });
        } else {
            jwt.verify(token, SECRET_KEY, (err) => {
                if (err) {
                    return res.json({ error: "Error in token" });
                } else {
                    next();
                }
            })
        }
    } catch (error) {
        res.json({ error: error.message, message: "Error while verifying the token" });
    }
}


export const changePassword = async (req, res) => {
    try {
        const { email, password, confirmPassword } = req.body;
        if (password.length < 1 || confirmPassword.length < 1) {
            return res.json({ warning: "Password must be greater than 5 characters" })
        } else if (password !== confirmPassword) {
            return res.json({ warning: "Password is not same" })
        } else {
            const user = await User.findOne({ email });
            if (!user) {
                return res.json({ error: "User not exist" })
            } else {
                const hashedPass = await hashedPassword(confirmPassword)
                await User.findOneAndUpdate(user, {
                    email,
                    password: hashedPass
                })
                return res.json({ success: "Password changed successfully" })
            }
        }

    } catch (error) {
        res.status(500).json(error, "error while changing the password")
    }
}


export const getCart = async (req, res) => {
    try {
        const { userID } = req.params;
        const user = await User.findById(userID);
        if (!user) {
            return res.json({ error: "Login to get the cart items" });
        } else {
            return res.json({ message: "items found", cart: user?.cart })
        }
    } catch (error) {
        res.json(error, "error while getting the cart items");
    }
}


export const deleteCartItem = async (req, res) => {
    try {
        const { itemTitle, userId } = req.body;
        const user = await User.findById(userId);
        if (!user) {
            return res.json({ error: "Login to delete the items" })
        }
        else {
            const { cart } = user;
            const itemIndex = cart.findIndex((item) => item.title.shortTitle === itemTitle)
            if (itemIndex >= 0) {
                cart.splice(itemIndex, 1);
                user.cart = cart;
                await user.save();
                return res.json({ success: "Removed from cart" });
            } else {
                return res.json({ error: "Item not found in the cart" });
            }
        }
    } catch (error) {
        res.json(error, "error while removing from cart")
    }
}



export const removeAllFromCart = async (req, res) => {
    try {
        const { userId } = req.body;
        const user = await User.findById(userId);
        if (!user) {
            return res.json({ error: "User not found" })
        } else {
            await User.findByIdAndUpdate(user._id, {
                cart: []
            })
            return res.json({ message: "Removed all", cart: user.cart })
        }
    } catch (error) {
        res.status(500).json(error, "error while removing from cart")
    }
}