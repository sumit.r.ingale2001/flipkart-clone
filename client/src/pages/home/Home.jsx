/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import { Box } from "@mui/material"
import { CarouselContainer, Discount, Header, Slider } from "../../components"
import { useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux'
import { getAllProducts, openModal } from "../../store/store";
import { verifyUser } from "../../services/api";
import { toast } from "react-toastify";
import { useGetUserId } from "../../hooks/useGetUserId";
import { useCookies } from "react-cookie";


const Home = () => {

    const dispatch = useDispatch();

    const [cookies] = useCookies(["access_token"])


    useEffect(() => {
        const verify = async () => {
            const response = await verifyUser()
            if (response) {
                if (!response.data.message === "Verified user") {
                    dispatch(openModal())
                } else {
                    console.log(response.data.message)
                }
            } else {
                toast.error("something went wrong");
            }
        }
        verify()
    }, [cookies])

    useEffect(() => {
        dispatch(getAllProducts());
    }, [])


    const data = useSelector((state) => state.products.allProducts);

    return (
        <>
            <Header />
            <Box sx={{ maxWidth: "1680px", m: "0 auto", overflow: "hidden" }}>
                <CarouselContainer />
                <Discount />
                <Slider data={data} title='Deal of the day' />
                <Slider data={data} title="Top selection" />
            </Box>
        </>
    )
}

export default Home
