/* eslint-disable no-unused-vars */
import { Box, Paper, styled } from '@mui/material'

export const Container = styled(Box)(({ theme }) => ({
    height: "70vh",
    width: "100vw",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
}))

export const StyledPaper = styled(Paper)(({ theme }) => ({
    padding: "1.5rem 2.2rem",
    maxWidth: "400px",
    width: "80%",
    "& > form": {
        display: "flex",
        flexDirection: "column",
        gap: "1.2rem",
    },
    "& >form> h5": {
        textAlign: "center",
        fontWeight: "bold",
    },
    "& div button": {
        background: "#fb641b",
        ":hover": {
            background: "#fb641b",
        }
    },
    "& > form > div": {
        display: "flex",
        justifyContent: "space=between",
        alignItems: "center",
        padding: "0 0.5rem",
        "& > div":{
            cursor:"pointer"
        }
    }
}))