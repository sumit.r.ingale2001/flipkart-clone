import { Box, Button, TextField, Typography } from "@mui/material"
import { Container, StyledPaper } from "./styles"
import { useState } from "react"
import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import { changePassword } from "../../services/api";
import { toast } from 'react-toastify'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { openModal } from '../../store/store'

const initialValues = {
    email: "",
    password: "",
    confirmPassword: ""
}

const Forgot = () => {

    const dispatch = useDispatch()
    const navigate = useNavigate()
    const [showPassword, setShowPassword] = useState(false);
    const [showSecondPass, setShowSecondPass] = useState(false)

    const [formValues, setFormValues] = useState(initialValues);



    const handleChange = (e) => {
        setFormValues({ ...formValues, [e.target.name]: e.target.value })
    }

    const handleClick = async () => {
        const response = await changePassword(formValues)
        if (response) {
            if (response.data.success) {
                toast.success(response.data.success);
                navigate("/")
                dispatch(openModal())
            } else if (response.data.warning) {
                toast.warn(response.data.warning);
            } else {
                toast.error(response.data.error);
            }
        } else {
            toast.error("Something went wrong");
        }
    }

    return (
        <>
            <Container>
                <StyledPaper>
                    <form>
                        <Typography variant="h5" >Forgot password?</Typography>
                        <Box>
                            <TextField
                                label="Enter your email"
                                variant="standard"
                                fullWidth
                                name="email"
                                type="email"
                                onChange={handleChange}
                            />
                        </Box>
                        <Box>
                            <TextField
                                name="password"
                                type={showPassword ? "text" : "password"}
                                label="Enter new password"
                                variant="standard"
                                onChange={handleChange}
                                fullWidth />

                            <Box onClick={() => setShowPassword(!showPassword)} >
                                {showPassword ? <VisibilityOffIcon /> : <VisibilityIcon />}
                            </Box>
                        </Box>
                        <Box>
                            <TextField
                                onChange={handleChange}
                                type={showSecondPass ? "text" : "password"}
                                name="confirmPassword"
                                label="confirm password"
                                variant="standard"
                                fullWidth />

                            <Box onClick={() => setShowSecondPass(!showSecondPass)}  >
                                {showSecondPass ? <VisibilityOffIcon /> : <VisibilityIcon />}
                            </Box>
                        </Box>
                        <Box>
                            <Button variant="contained" onClick={handleClick} fullWidth>Change password</Button>
                        </Box>
                    </form>
                </StyledPaper>
            </Container>
        </>
    )
}

export default Forgot
