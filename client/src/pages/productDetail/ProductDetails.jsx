import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { getProduct, openModal } from "../../store/store";
import { Box, Button, Grid, TableBody, TableCell, TableRow, Typography } from '@mui/material'
import { GridContainer, Left, PriceContent, Right, SmallText, StyledBadge } from "./styles";
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import FlashOnIcon from '@mui/icons-material/FlashOn';
import { addToCart} from "../../services/api";
import { toast } from 'react-toastify'
import { useGetUserId } from "../../hooks/useGetUserId";
import { payment } from "../../payments/payments";


const ProductDetails = () => {
    const date = new Date(new Date().getTime() + (5 * 24 * 60 * 60 * 1000))
    const adURL = 'https://rukminim1.flixcart.com/lockin/774/185/images/CCO__PP_2019-07-14.png?q=50';
    const dispatch = useDispatch();
    const { id } = useParams();
    const userID = useGetUserId();
    const navigate = useNavigate();


    const data = useSelector((state) => state.products.product)
    useEffect(() => {
        dispatch(getProduct(id))
    }, [id])

    const addProductToCart = async (id, userId) => {
        const response = await addToCart({ id, userId })
        if (response) {
            if (response.data.info) {
                toast.info(response.data.info)
                dispatch(openModal())
            } else {
                toast.success(response.data.success)
                navigate('/cart')
            }
        } else {
            toast.error("Something went wrong")
        }
    }



    return (
        <>
            <GridContainer container >
                <Grid item xs={12} lg={4} md={12} sm={12}>
                    <Left>
                        <img src={data.detailUrl} alt="product" width="100%" />
                        <Box>
                            <Button variant="contained" onClick={() => addProductToCart(data._id, userID)} fullWidth sx={{ background: "#ff9f00", ":hover": { background: "#ff9700" } }} > <ShoppingCartIcon />&nbsp;Add to cart</Button>
                            <Button variant="contained" onClick={() => payment(data?.price?.cost)} fullWidth sx={{ background: "#fb5413", ":hover": { background: "#fb5413" } }} > <FlashOnIcon />&nbsp;Buy now</Button>
                        </Box>
                    </Left>
                </Grid>
                <Grid item xs={12} lg={8} md={12} sm={12}>
                    <Right>
                        <Typography variant="h5" >{data?.title?.longTitle}</Typography>
                        <Typography>8 Ratings and 1 reviews</Typography>
                        <PriceContent>
                            <span>₹{data?.price?.cost}</span>&nbsp;&nbsp;
                            <span> <strike>{data?.price?.mrp}</strike></span>&nbsp;&nbsp;&nbsp;
                            <span>{data?.price?.discount}</span>
                        </PriceContent>
                        <Typography>Available Offers</Typography>
                        <SmallText>
                            <Typography><StyledBadge /> Get extra 10% off upto ₹50 on 1 item(s) T&C </Typography>
                            <Typography><StyledBadge /> Get extra 13% off (price inclusive of discount) T&C </Typography>
                            <Typography><StyledBadge /> Sign up for FlipKart PayLater and get FlipKart Gift Card worth ₹100 know more </Typography>
                            <Typography><StyledBadge /> Bank offer 9% Cashback on FlipKart Axis Bank Card T&C</Typography>
                            <Typography><StyledBadge /> No on EMI on Bajaj Finserv EMI card on cart value above ₹2999 T&C</Typography>
                        </SmallText>
                        <TableBody>
                            <TableRow>
                                <TableCell>Delivery</TableCell>
                                <TableCell>Delivery by {date.toDateString()}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Warranty</TableCell>
                                <TableCell>No Warranty</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Seller</TableCell>
                                <TableCell  >
                                    <Box component='span' style={{ color: "#2874f0" }} >
                                        SuperComNet
                                    </Box>
                                    <Typography>GST invoice available</Typography>
                                    <Typography>View more sellers starting from ₹{data?.price?.cost} </Typography>
                                </TableCell>

                            </TableRow>
                            <TableRow>
                                <TableCell colSpan={2} >
                                    <img src={adURL} alt="advertisement" style={{ width: 300 }} />
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>{data.description ? "Description" : null}</TableCell>
                                <TableCell>{data.description ? data.description : null}</TableCell>
                            </TableRow>
                        </TableBody>
                    </Right>
                </Grid>
            </GridContainer>
        </>
    )
}

export default ProductDetails;
