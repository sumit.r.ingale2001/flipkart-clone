export {default as Home} from './home/Home'
export {default as ProductDetails} from "./productDetail/ProductDetails"
export {default as Cart} from './cart/Cart'
export {default as Forgot} from './forgotPassword/Forgot'