import { KEY } from "../keys/data"
import { handlePayment, verifyPayment } from "../services/api"



const handleOpenRazorPay = (data) => {
    const options = {
        key: KEY,
        amount: Number(data.amount) * 100,
        currency: data.currency,
        name: "FLIPKART CLONE",
        order_id: data.id,
        handler: async function (response) {
            console.log(response)
            try {
                const { data } = await verifyPayment(response)
                console.log(data)
            } catch (error) {
                console.log(error)
            }

        }

    }
    const rzp = new window.Razorpay(options)
    rzp.open();
}

export const payment = async (amt) => {
    const response = await handlePayment(amt)
    if (response) {
        if (response.data.data.error) {
            console.log(response.data.data.error)
        } else {
            console.log(response.data.data.success)
            handleOpenRazorPay(response.data.data)
        }
    } else {
        console.log("Something went wrong");
    }
}