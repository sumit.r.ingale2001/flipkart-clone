import { Box } from "@mui/material"
import { Dialog, Footer, Navbar } from "./components/index"
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { Cart, Forgot, Home, ProductDetails } from "./pages/index"
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useCookies } from "react-cookie";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { openModal } from "./store/store";


function App() {

  const [cookies] = useCookies(["access_token"])

  const dispatch = useDispatch()
  useEffect(() => {
    if (!cookies.access_token) {
      dispatch(openModal())
    }
  }, [])

  return (

    <Router>
      <ToastContainer
        position="top-center"
        autoClose={1500}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="colored"

      />
      <Navbar />
      <Dialog />
      <Box sx={{ mt: "60px" }}>
        <Routes>
          <Route path="/" exact element={<Home />} />
          <Route path="/product/:id" exact element={<ProductDetails />} />
          <Route exact path="/cart" element={<Cart />} />
          <Route exact path="/forgot" element={<Forgot />} />
        </Routes>
      </Box>
      <Footer />
    </Router>

  )
}

export default App
