/* eslint-disable react/prop-types */
import { Box, Paper, Typography } from "@mui/material"
import { Container, MainContainer, Middle, ProductLink, SideProduct } from "./styles"
import Carousel from "react-multi-carousel"
import "react-multi-carousel/lib/styles.css";
import { Link } from "react-router-dom";

const responsive = {
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 3
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1
    }
};





const Slider = ({ data, title }) => {
    return (
        <>
            <MainContainer >
                <Typography variant='h6' >{title}</Typography>
                <Container>
                    <SideProduct  >
                        <Box sx={{
                            background: "url('https://rukminim1.flixcart.com/fk-p-flap/530/810/image/1758140ba2b1da70.jpg?q=20')",
                        }} ></Box>
                    </SideProduct>
                    <Middle>
                        <Carousel
                            responsive={responsive}
                            infinite={true}
                            swipeable={true}
                            draggable={true}
                            autoPlay={true}
                            centerMode={true}
                            autoPlaySpeed={4000}
                            dotListClass='custom-dot-list-style'
                            itemClass='Carousel-item-padding-40-px'
                            containerClass='carousel-container'

                        >
                            {
                                data.map((item) => (
                                    <ProductLink component={Link} key={item.id} to={`/product/${item.id}`} >
                                        <Paper>
                                            <img src={item.url} alt="product image" />
                                            <Typography sx={{ fontWeight: "600", color: "#212121" }} >{item.title.shortTitle}</Typography>
                                            <Typography sx={{ color: "green" }} >{item.discount}</Typography>
                                            <Typography sx={{ color: "#212121", opacity: "0.6" }} >{item.tagline}</Typography>
                                        </Paper>
                                    </ProductLink>
                                ))
                            }
                        </Carousel>
                    </Middle>
                    <SideProduct>
                        <Box sx={{
                            background: "url('https://rukminim1.flixcart.com/fk-p-flap/530/810/image/d133935e34408b02.jpg?q=20')",
                        }} ></Box>
                    </SideProduct>
                </Container>
            </MainContainer>
        </>
    )
}

export default Slider
