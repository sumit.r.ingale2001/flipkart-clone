/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import { Menu, MenuItem } from '@mui/material'
import { useCookies } from 'react-cookie';
import { useDispatch } from 'react-redux';
import { openModal } from '../../store/store';
import { toast } from 'react-toastify'
import PowerSettingsNewIcon from '@mui/icons-material/PowerSettingsNew';


const Logout = ({ open, handleClose, setAccount }) => {

    const [cookies, setCookies, removeCookies] = useCookies(["access_token"]);

    const dispatch = useDispatch();

    const handleLogout = () => {

        toast.success("Logged out successfully")
        removeCookies("access_token",);
        window.localStorage.clear("UserID");
        window.localStorage.clear("username");
        dispatch(openModal())
        setAccount(localStorage.getItem("username"))

    }
    const openMenu = Boolean(open);

    return (
        <Menu
            sx={{ marginTop: "0.5rem" }}
            open={openMenu}
            anchorEl={open}
            onClose={handleClose}
            MenuListProps={{
                'aria-labelledby': 'basic-button'
            }}
        >
            <MenuItem sx={{ display: "flex", alignItems: "center", gap: "20px" }} onClick={handleLogout} >Logout&nbsp;<PowerSettingsNewIcon fontSize='small' /></MenuItem>
        </Menu>
    )
}

export default Logout
