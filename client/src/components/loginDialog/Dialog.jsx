/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import Modal from '@mui/material/Modal';
import { Box, Button, Grid, TextField, Typography } from '@mui/material';
import { Container, Left, Right } from './styles';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { closeModal } from '../../store/store';
import { loginUser, signupUser } from '../../services/api';
import { useGlobalContext } from '../../context/context';
import { toast } from 'react-toastify'
import { useCookies } from 'react-cookie';
import { useNavigate } from 'react-router-dom'

const initialSignupValues = {
  phone: "",
  email: "",
  password: "",
  firstName: "",
  lastName: "",
  username: ""
}
const initialLoginValues = {
  name: "",
  email: "",
  password: ""
}
const bg = "https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/login_img_c4a81e.png"

const Dialog = () => {
  const [loginValues, setLoginValues] = useState(initialLoginValues)
  const [signupValues, setSignupValues] = useState(initialSignupValues)
  const [toggle, setToggle] = useState(false);
  const [cookie, setCookie] = useCookies(["access_token"]);
  const [password, setPassword] = useState(false)

  const navigate = useNavigate()
  const { setAccount } = useGlobalContext();

  const dispatch = useDispatch()
  const { isOpen } = useSelector((state) => state.modal);

  const handleLogin = async (e) => {
    e.preventDefault();
    setPassword(false)
    const response = await loginUser(loginValues);
    if (response) {
      if (response.data.error) {
        toast.error(response.data.error)
      } else if (response.data.warning) {
        toast.warn(response.data.warning);
      } else {
        toast.success(`${response.data.message}, Welcome ${response.data.username}`);
        setLoginValues(initialLoginValues)
        localStorage.setItem("username", response.data.username)
        localStorage.setItem("email", response.data.email)
        const username = localStorage.getItem("username")
        setAccount(username)
        dispatch(closeModal())
        setCookie("access_token", response.data.token)
        window.localStorage.setItem("UserId", response.data.userID)
      }
    }
  }

  const handleSignup = async (e) => {
    e.preventDefault();
    setPassword(false)
    const response = await signupUser(signupValues)
    if (response) {
      if (response.data.error) {
        toast.error(response.data.error);
      } else if (response.data.warning) {
        toast.warn(response.data.warning)
      } else {
        toast.success(`${response.data.message}, Please login now`);
        setToggle(false)
        setSignupValues(initialSignupValues)
      }
    } else {
      toast.error("Something went wrong try again later")
    }

  }


  return (
    <Modal
      open={isOpen}
      onClose={() => dispatch(closeModal())}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Container>
        <Grid container height='100%' >
          {
            !toggle ? (
              <>
                <Left item xs={5}>
                  <Box>
                    <Typography variant='h5' >Login</Typography>
                    <span>Get access to your Orders,<br /> Wishlist and Recommendations</span>
                  </Box>
                  <img src={bg} alt="login image" />
                </Left>
                <Grid item xs={7} >
                  <Right>
                    <Box sx={{ gap: "15px" }}>
                      <TextField
                        label='Enter your name'
                        fullWidth variant="standard"
                        name='name'
                        type='text'
                        value={loginValues.name}
                        onChange={(e) => setLoginValues({ ...loginValues, [e.target.name]: e.target.value })} />

                      <TextField
                        label='Enter email'
                        value={loginValues.email}
                        fullWidth
                        variant="standard"
                        name='email'
                        type='email'
                        onChange={(e) => setLoginValues({ ...loginValues, [e.target.name]: e.target.value })} />

                      <TextField
                        label='Enter password'
                        fullWidth
                        variant="standard"
                        value={loginValues.password}
                        name='password' type={password ? "text" : "password"}
                        onChange={(e) => setLoginValues({ ...loginValues, [e.target.name]: e.target.value })} />

                      <Box sx={{ fontSize: "0.8rem", display: "flex", alignItems: "center" }} >
                        <input type="checkbox" onClick={() => setPassword(!password)} />&nbsp;
                        <span>
                          {password ? "Hide" : "Show"}
                        </span>
                      </Box>
                      <Typography>By continuing, you agree to Flipkart <span>Terms of Use</span> and <span>Privacy Policy.</span></Typography>
                      <Button sx={{ background: "#fb641b", ":hover": { background: "#fb641b" } }} variant='contained' fullWidth onClick={handleLogin}>Login</Button>
                      <Button onClick={() => { navigate("/forgot"), dispatch(closeModal()) }} >Forgot password</Button>
                    </Box>
                    <Typography onClick={() => { setToggle(true); setLoginValues(initialLoginValues); setPassword(false) }} >New to Flipkart? Create an account</Typography>
                  </Right>
                </Grid>
              </>
            ) : (
              <>
                <Left item xs={5} >
                  <Box>
                    <Typography variant='h5' >Looks like you are new here!</Typography>
                    <span>Sign up to get started</span>
                  </Box>
                  <img src={bg} alt="login image" />
                </Left>
                <Grid item xs={7} >
                  <Right >
                    <Box sx={{ justifyContent: "space-between", gap: "10px" }}>
                      <TextField
                        label='Enter first name'
                        fullWidth variant="standard"
                        type='text' name='firstName'
                        value={signupValues.firstName}
                        onChange={(e) => setSignupValues({ ...signupValues, [e.target.name]: e.target.value })} />

                      <TextField
                        label='Enter last name'
                        fullWidth
                        variant="standard"
                        type='text'
                        name='lastName'
                        value={signupValues.lastName}
                        onChange={(e) => setSignupValues({ ...signupValues, [e.target.name]: e.target.value })} />

                      <TextField
                        label='Enter username'
                        fullWidth
                        variant="standard"
                        type='text'
                        name='username'
                        value={signupValues.username}
                        onChange={(e) => setSignupValues({ ...signupValues, [e.target.name]: e.target.value })} />

                      <TextField
                        label='Enter Mobile number'
                        fullWidth variant="standard"
                        type='number'
                        name='phone'
                        value={signupValues.phone}
                        onChange={(e) => setSignupValues({ ...signupValues, [e.target.name]: e.target.value })} />

                      <TextField label='Enter email'
                        fullWidth
                        variant="standard"
                        type='email'
                        name='email'
                        value={signupValues.email}
                        onChange={(e) => setSignupValues({ ...signupValues, [e.target.name]: e.target.value })} />

                      <TextField
                        label='Enter password'
                        value={signupValues.password}
                        fullWidth
                        variant="standard"
                        type={password ? "text" : "password"} name='password' onChange={(e) => setSignupValues({ ...signupValues, [e.target.name]: e.target.value })} />

                      <Box sx={{ fontSize: "0.8rem", display: "flex", alignItems: "center" }} >
                        <input type="checkbox" onClick={() => setPassword(!password)} />&nbsp;
                        <span>
                          {password ? "Hide" : "Show"}
                        </span>
                      </Box>

                      <Typography>By continuing, you agree to Flipkart <span>Terms of Use</span> and <span>Privacy Policy.</span></Typography>
                      <Button sx={{ background: "#fb641b", ":hover": { background: "#fb641b" } }} variant='contained' fullWidth onClick={handleSignup} >Sign up</Button>
                      <Button fullWidth onClick={() => { setToggle(false); setSignupValues(initialSignupValues), setPassword(false) }} >Existing user? Login</Button>
                    </Box>
                  </Right>
                </Grid>
              </>
            )
          }
        </Grid>
      </Container>
    </Modal>
  );
}

export default Dialog