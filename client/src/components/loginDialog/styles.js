/* eslint-disable no-unused-vars */
import { Box, Grid, styled } from "@mui/material";


export const Container = styled(Box)(({ theme }) => ({
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    maxWidth: "800px",
    width: "95%",
    height: "75vh",
    background: "#fff",
    [theme.breakpoints.down("md")]:{
        height:"80vh"
    }
}))

export const Left = styled(Grid)(({ theme }) => ({
    height: "100%",
    background: "#2874f0",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "column",
    padding: "2rem 0",
    "& > img": {
        objectFit: "contain",
        width: "85%",
    },
    "& > div": {
        width: "85%",
        "& > h5": {
            color: "#fff",
            marginBottom: "1rem",
            fontWeight: "bold",
        },
        "& > span": {
            fontSize: "1rem",
            color: "lightgray",
            [theme.breakpoints.down("md")]: {
                fontSize: "0.8rem"
            }
        }
    }
}))

export const Right = styled("form")(({ theme }) => ({
    padding: "1.5rem",
    width: "100%",
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    "& > p": {
        textAlign: "center",
        cursor: "pointer",
        color: "#2874f0",
        fontSize: "14px",
        [theme.breakpoints.down("md")]:{
            fontSize:"12px"
        }
    },
    "& > div": {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        "& > button": {
            boxShadow: "none",
            [theme.breakpoints.down("md")]: {
                fontSize: "0.8rem"
            }
        },
        "& > p": {
            fontSize: "12px",
            color: "#878787",
            [theme.breakpoints.down("md")]: {
                fontSize: "10px"
            },
            "& > span": {
                color: "#2874f0"
            }
        }
    }
}))