/* eslint-disable no-unused-vars */
import { Container, Image, ItemContainer } from "./styles"
import { headerData } from "../../utils/constants"
import { Box, Menu, MenuItem, Typography } from "@mui/material"
import { useState } from "react"



const Header = () => {

    const [open, setOpen] = useState(null);
    const openMenu = Boolean(open)

    const handleClose = () => {
        setOpen(null)
    }

    const handleOpen = (e) => {
        setOpen(e.currentTarget);
    }
    return (
        <Container>
            <ItemContainer>
                {
                    headerData.map((item, index) => (
                        <>
                            <Box key={index} onClick={handleOpen} >
                                <Image>
                                    <img src={item.url} alt="categories" />
                                </Image>
                                <Typography>{item.text}</Typography>
                            </Box>
                        </>
                    ))
                }
            </ItemContainer>
        </Container>
    )
}

export default Header
