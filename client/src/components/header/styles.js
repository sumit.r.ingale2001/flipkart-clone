/* eslint-disable no-unused-vars */
import { Box, styled } from "@mui/material";



export const Container = styled(Box)(({ theme }) => ({
    height: "126px",
    width: "100vw",
    overflowX: "scroll",
    overflowY: "hidden",
    background: "white",
    margin: "3.5rem auto 0 auto",
    boxShadow: "rgba(0, 0, 0, 0.02) 0px 1px 3px 0px, rgba(27, 31, 35, 0.15) 0px 0px 0px 1px",

    "p": {
        fontSize: "14px",
        fontWeight: "bold",
        textAlign: "center"
    },
    "::-webkit-scrollbar": {
        width: 0
    }
}))

export const Image = styled(Box)(({ theme }) => ({
    height: "80px",
    width: "80px",
    overflow: "hidden",
    borderRadius: "50%",
    "& > img": {
        objectFit: "cover",
        height: "100%",
        width: "100%",
    },

}))

export const ItemContainer = styled(Box)(({ theme }) => ({
    maxWidth: "1280px",
    height: "100%",
    display: "flex",
    margin: "0 auto",
    justifyContent: "space-between",
    alignItems: "center",
    "& > div": {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        cursor: "pointer",
        ":hover": {
            color: "#2974f1"
        }
    },
}))