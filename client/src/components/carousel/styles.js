/* eslint-disable no-unused-vars */
import { Box, styled } from "@mui/material";


export const Container = styled(Box)(({ theme }) => ({
    width: "100%",
    "& > div": {
        height: "100%",
        width: "100%",
    },
    "& img": {
        height: "100%",
        width: "100%",
        objectFit: "cover",
    },
    [theme.breakpoints.down("md")]:{
        "& img":{
            height:"250px",
            objectPosition:"center"
        }
    }
}))