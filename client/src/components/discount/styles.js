/* eslint-disable no-unused-vars */
import { Box, styled } from "@mui/material";


export const Container = styled(Box)(({theme})=>({
    width:"100vw",
    height:"140px",
    padding:"2px 1rem",
    background:"white",
    display:"flex",
    justifyContent:"center",
    alignItems:"center",
    "& > img":{
        height:"100%",
        width:"100%",
        objectFit:"cover"
    },
}))