import { Badge, Button, MenuItem } from "@mui/material"
import { ButtonContainer, OtherLinkContainer } from "./styles"
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { useGlobalContext } from "../../context/context";
import Profile from "../profile/Profile";
import { openModal } from "../../store/store";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useGetUserId } from "../../hooks/useGetUserId";

const Navlinks = () => {
    const { account, setAccount } = useGlobalContext();
    const userId = useGetUserId()

    const dispatch = useDispatch()

    const cartItems = useSelector((state) => state.products.cart)

    const navigate = useNavigate()

    return (
        <OtherLinkContainer>
            <MenuItem onClick={() => navigate("/")} >Home</MenuItem>
            <MenuItem>Become a seller</MenuItem>
            <MenuItem>More<ExpandMoreIcon /></MenuItem>
            <MenuItem onClick={() => navigate("/cart")} >
                <Badge color="secondary" badgeContent={userId && cartItems?.length}  >
                    Cart&nbsp;<ShoppingCartIcon />
                </Badge>
            </MenuItem>
            {
                account ? <Profile account={account} setAccount={setAccount} /> : (
                    <ButtonContainer onClick={() => dispatch(openModal())} >
                        <Button size="medium" variant="contained">Login</Button>
                    </ButtonContainer>
                )
            }
        </OtherLinkContainer>
    )
}

export default Navlinks
