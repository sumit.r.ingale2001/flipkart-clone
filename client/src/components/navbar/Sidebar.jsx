/* eslint-disable react/prop-types */
import { Drawer } from "@mui/material"
import Navlinks from "./Navlinks"
import CloseIcon from '@mui/icons-material/Close';

const Sidebar = ({ open, handleClose }) => {
    return (
        <Drawer
            sx={{ position: "relative" }}
            open={open}
            onClose={handleClose}
        >
            <CloseIcon onClick={handleClose} fontSize="large" sx={{ position: "absolute", color: "#2974f1", right: "10px", top:"10px" }} />
            <Navlinks />
        </Drawer>
    )
}

export default Sidebar
