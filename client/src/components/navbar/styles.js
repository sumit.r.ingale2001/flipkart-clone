/* eslint-disable no-unused-vars */
import { AppBar, Box, Drawer, Paper, styled, Toolbar } from '@mui/material'

export const StyledToolbar = styled(Toolbar)(({ theme }) => ({
    display: "flex",
    alignItems: "center",
    gap: "2rem",
    width: "80%",
    maxWidth: "1400px",
    margin: "0 auto",
    [theme.breakpoints.down("md")]: {
        width: "100%",
        gap: "1rem",
    },
    [theme.breakpoints.down("sm")]: {
        width: "100%",
        gap: "0.7rem",
    }
}));


export const Header = styled(AppBar)(({ theme }) => ({
    backgroundColor: "#2974f1",
}))

export const LogoContainer = styled(Box)(({ theme }) => ({
    textDecoration: "none",
    "& > img": {
        width: "75px",
        [theme.breakpoints.down("md")]: {
            width: "60px"
        },
        [theme.breakpoints.down("sm")]: {
            width: "40px"
        }

    },
    "& > p": {
        fontSize: "12px",
        fontStyle: "italic",
        position: "relative",
        color: "white",
        [theme.breakpoints.down("md")]: {
            fontSize: "10px"
        },
        [theme.breakpoints.down("sm")]: {
            fontSize: "7px"
        }
    },
    "& p>span": {
        color: "yellow",
        fontWeight: "bold"
    },
    "& div>img": {
        width: "10px",
        position: "absolute",
        right: "-6px",
        bottom: "5px",
        [theme.breakpoints.down("sm")]: {
            width: "6px",
            right:"0"
        }
    },
}))

export const InputContainer = styled(Box)(({ theme }) => ({
    flex: 2,
    background: "white",
    display: "flex",
    alignItems: "center",
    height: "36px",
    position: "relative",
    borderRadius: "3px",
    "& > input": {
        height: "100%",
        width:"100%",
        padding: "0 1rem",
        border: "none",
        outline: "none",
        borderRadius: "3px 0 0 3px"
    },
    "& > svg": {
        color: "#2974f1",
        marginRight: "1rem",
        cursor: "pointer",
    },
}))


export const ButtonContainer = styled(Box)(({ theme }) => ({
    position: "relative",
    "& > button": {
        color: "#2974f1",
        background: "white",
        padding: "5px 20px",
        borderRadius: "2px",
        marginLeft: "1rem",
        boxShadow: "none",
        ":hover": {
            boxShadow: "none",
            color: "white"
        }
    },
}));

export const NavlinksContainer = styled(Box)(({ theme }) => ({
    [theme.breakpoints.down("md")]: {
        display: "none"
    }
}))

export const OtherLinkContainer = styled(Box)(({ theme }) => ({
    display: "flex",
    gap: "1rem",
    alignItems: "center",
    "& > li": {
        fontSize: "0.9rem"
    },
    [theme.breakpoints.down("md")]: {
        flexDirection: "column",
        alignItems: "start",
        padding: "1rem 5rem 1rem 1rem",
        gap: "10px",
        "& > li": {
            fontSize: "1.2rem",
            color: "#2974f1",
        }
    }
}))

export const Wrapper = styled(Paper)(({ theme }) => ({
    background: "white",
    width: "100%",
    overflowY: "scroll",
    overflowX: "hidden",
    top: "2.5em",
    left: 0,
    position: "absolute",
    zIndex: 10,
    "& > li": {
        padding: "0.5rem 1rem",
        "& > a": {
            textDecoration: "none",
            color: "#1976d2",
            display: "flex",
            alignItems: "center",
            gap: "1rem",
            "& > p": {
                [theme.breakpoints.down("md")]: {
                    fontSize: "0.8rem"
                }
            },
            "& > img": {
                width: "50px",
                height: "50px",
                objectFit: "cover",
                [theme.breakpoints.down("md")]: {
                    height: "25px",
                    width: "25px"
                }
            }
        },
        ":hover": {
            backgroundColor: "#e0e0e0"
        }
    },
}))

export const DrawerBtn = styled(Box)(({ theme }) => ({
    display: "none",
    "& svg": {
        color: "white",
        fontSize: "2rem",
    },
    [theme.breakpoints.down("md")]:{
        display:"block"
    }
}))
