import { useEffect, useState } from "react";
import { InputContainer, Wrapper } from "./styles"
import SearchIcon from '@mui/icons-material/Search';
import { useDispatch, useSelector } from "react-redux";
import { getAllProducts } from "../../store/store";
import { ListItem, Typography } from "@mui/material";
import { Link } from "react-router-dom";


const Search = () => {
    const [text, setText] = useState(null)

    const dispatch = useDispatch()
    const { allProducts } = useSelector((state) => state.products)

    useEffect(() => {
        dispatch(getAllProducts())
    }, [dispatch])



    const getText = (text) => {
        setText(text)
    }

    return (
        <>
            <InputContainer>
                <input type="text"
                    onChange={(e) => getText(e.target.value)}
                    value={text}
                    placeholder="Search for products and more" />
                <SearchIcon />
                {
                    text && (
                        <Wrapper>
                            {
                                allProducts.filter((product) => (
                                    product.title.longTitle.toLowerCase().includes(text.toLowerCase()) || product.title.shortTitle.toLowerCase().includes(text.toLowerCase())
                                )).map((product) => (
                                    <ListItem key={product.id} >
                                        <Link to={`/product/${product.id}`} onClick={() => setText(null)} >
                                            <img src={product.url} alt={product.name} />
                                            <Typography>{product.title.longTitle}</Typography>
                                        </Link>
                                    </ListItem>
                                ))
                            }
                        </Wrapper>
                    )
                }
            </InputContainer>
        </>
    )
}

export default Search
