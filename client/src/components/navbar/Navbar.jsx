/* eslint-disable react/prop-types */
import { DrawerBtn, Header, LogoContainer, NavlinksContainer, StyledToolbar } from "./styles"
import { Box, IconButton, Typography } from '@mui/material'
import logo from '../../assets/flipkartLogo.png'
import plus from '../../assets/plus.png'
import MenuIcon from '@mui/icons-material/Menu';
import Search from './Search'
import { Link } from "react-router-dom"
import Navlinks from "./Navlinks"
import { useState } from "react";
import Sidebar from "./Sidebar";



const Navbar = () => {


    const [open, setOpen] = useState(false)

    const handleClose = () => {
        setOpen(false)
    }

    return (
        // <ExpandMore />
        <>
            <Header>
                <StyledToolbar>

                    {/* logo section start  */}
                    <LogoContainer component={Link} to="/" >
                        <img src={logo} alt="flipkart logo" />
                        <Typography>
                            Explore&nbsp;
                            <Box component='span'>Plus</Box>
                            <Box><img src={plus} alt="plus icon" /></Box>
                        </Typography>
                    </LogoContainer>
                    {/* logo section end  */}


                    {/* input container start  */}
                    <Search />
                    {/* input container end */}


                    {/* drawer menu  */}
                    <DrawerBtn>
                        <IconButton onClick={() => setOpen(true)} >
                            <MenuIcon />
                        </IconButton>
                    </DrawerBtn>

                    {/* other links start  */}
                    <NavlinksContainer>
                        <Navlinks />
                    </NavlinksContainer>
                    {/* other links end */}
                </StyledToolbar>
                <Sidebar open={open} handleClose={handleClose} />
            </Header>
        </>
    )
}

export default Navbar
