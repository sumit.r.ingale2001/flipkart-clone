/* eslint-disable react/prop-types */
import { useState } from "react"
import Logout from "../logout/Logout"
import { MenuItem } from "@mui/material"
import PowerSettingsNewIcon from '@mui/icons-material/PowerSettingsNew';


const Profile = ({ account, setAccount }) => {


    const slicedName = (word) => {
        return word.slice(0, 12)
    }

    const [open, setOpen] = useState(null);


    const handleClose = () => {
        setOpen(null)
    }

    const handleOpen = (e) => {
        setOpen(e.currentTarget)
    }

    return (
        <>


            <MenuItem onClick={handleOpen} size="medium" variant="contained" >{`${slicedName(account)}...`}&nbsp;&nbsp;<PowerSettingsNewIcon fontSize='small' /></MenuItem>
            <Logout open={open} handleClose={handleClose} setAccount={setAccount} />
        </>
    )
}

export default Profile
