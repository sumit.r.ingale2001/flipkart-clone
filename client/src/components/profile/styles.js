/* eslint-disable no-unused-vars */
import { Box, styled } from "@mui/material";


export const Container = styled(Box)(({theme})=>({
    cursor:"pointer",
    fontSize: "0.8rem",
    padding: "5px 20px",
    [theme.breakpoints.down("md")]:{
        color:"white",
        background:"#2947f1"
    }
}))