/* eslint-disable react/prop-types */
import { Menu, MenuItem } from "@mui/material"
import { fashion } from "../../utils/constants"

const FashionMenu = ({ open, handleClose }) => {

    const openMenu = Boolean(open)
    return (
        <>
            <Menu
                sx={{ mt: "0.5rem" }}
                id="basic-menu"
                anchorEl={open}//e.currentTarget
                open={openMenu}
                onClose={handleClose}
                MenuListProps={{
                    'aria-labelledby': 'basic-button'
                }}
            >
                {
                    fashion.map((link) => (
                        <MenuItem key={link.id} >
                            {link.text}
                        </MenuItem>
                    ))
                }
            </Menu>
        </>
    )
}

export default FashionMenu
